#! /bin/bash
# Grabs the binaries for Blob-Bounce and makes release archives for Windows and Linux

zip Blob-Bounce-Windows.zip Blob-Bounce-Windows.exe Blob-Bounce-Windows.pck
tar cfJ Blob-Bounce-Linux.tar.xz Blob-Bounce-Linux.x86_64 Blob-Bounce-Linux.pck
rm Blob-Bounce-Linux.x86_64 Blob-Bounce-Linux.pck Blob-Bounce-Windows.exe Blob-Bounce-Windows.pck
ls -lart

